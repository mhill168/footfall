/**
 * Created by matthewhill on 08/09/15.
 */

(function () {

  'use strict';

  angular.module('footFall')
    .controller('FormController', FormController);

  FormController.$inject = ['$scope', '$state', '$rootScope', 'localStorageService', '$timeout'];

  function FormController($scope, $state, $rootScope, localStorageService, $timeout) {

    $scope.viewTitle = "Form";

    // listen for the $ionicView.enter event:
    $scope.$on('$ionicView.enter', function(e) {});

    var inStore = localStorageService.get('customers');
    $rootScope.customers = inStore || [];

    $scope.$watch('customers', function () {
    localStorageService.set('customers', $rootScope.customers);
    }, true);

    $scope.formData = {};

    $scope.processData = function () {
        if($scope.formData) {
            console.log($scope.formData);
            $state.go('main.customers');
        }
    }

  }

})();
