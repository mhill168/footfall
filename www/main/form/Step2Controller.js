/**
 * Created by matthewhill on 08/09/15.
 */

(function () {

  'use strict';

  angular.module('footFall')
    .controller('Step2Controller', Step2Controller);

  Step2Controller.$inject = ['$scope', '$rootScope', '$timeout'];

  function Step2Controller($scope, $rootScope, $timeout) {

      //Contains the filter options
      $scope.filterOptions = {
          vehicles: [
              {id : 0, vehicle : 'B-Max' },
              {id : 1, vehicle : 'C-Max' },
              {id : 2, vehicle : 'EcoSport' },
              {id : 3, vehicle : 'Fiesta' },
              {id : 4, vehicle : 'Focus' },
              {id : 5, vehicle : 'Galaxy' },
              {id : 6, vehicle : 'Ka' }
          ]
      };

      //Mapped to the model to filter
      $scope.filterItem = {
          vehicle: $scope.filterOptions.vehicles[0]
      };
  }

})();
