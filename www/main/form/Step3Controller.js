/**
 * Created by matthewhill on 08/09/15.
 */

(function () {

  'use strict';

  angular.module('footFall')
    .controller('Step3Controller', Step3Controller);

  Step3Controller.$inject = ['$scope', '$rootScope', 'localStorageService', '$timeout'];

  function Step3Controller($scope, $rootScope, localStorageService, $timeout) {

      $scope.formData.opinion = 33;

  }

})();
