
(function () {

    'use strict';

    angular
        .module('footFall', ['ionic', 'LocalStorageModule'])
        .config(config)
        .run(run);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', 'localStorageServiceProvider'];
    function config($stateProvider, $urlRouterProvider, $httpProvider, localStorageServiceProvider) {

        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        localStorageServiceProvider.setPrefix('cust');


        $stateProvider

            .state('main', {
                url: '/main',
                abstract: true,
                templateUrl: 'main//menu/menu.html',
                controller: 'MenuController'
            })

          .state('main.customers', {
            url: '/customers',
            views: {
              'main': {
                templateUrl: 'main/customers/customers.html',
                controller: 'CustomersController'
              }
            }
          })

          .state('main.customer', {
            url: '/customer/:id',
            views: {
              'main': {
                templateUrl: 'main/customer/customer.html',
                controller: 'CustomerController'
              }
            }
          })

          // route to show our basic form (/form)
          .state('main.form', {
            url: '/form',
            views: {
              'main': {
                templateUrl: 'main/form/form.html',
                controller: 'FormController'
              }
            }
          })

          // nested states
          // each of these sections will have their own view
          // url will be nested (/form/profile)
          .state('main.form.step1', {
            url: '/step-1',
            views: {
              'steps': {
                templateUrl: 'main/form/step-1.html',
                 controller: 'Step1Controller'
              }
            }
          })

          // url will be /form/interests
          .state('main.form.step2', {
            url: '/step-2',
            views: {
              'steps': {
                templateUrl: 'main/form/step-2.html',
                controller: 'Step2Controller'
              }
            }
          })

          // url will be /form/payment
          .state('main.form.step3', {
            url: '/step-3',
            views: {
              'steps': {
                templateUrl: 'main/form/step-3.html',
                  controller: 'Step3Controller'
              }
            }
          });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/main/customers');
    }

    run.$inject = ['$ionicPlatform', '$rootScope'];
    function run($ionicPlatform, $rootScope) {

        angular.element(document).on("click", function(e) {
            $rootScope.$broadcast("documentClicked", angular.element(e.target));
        });

        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    }

})();
