/**
 * Created by mhill168 on 07/09/15.
 */

(function () {

    'use strict';

    angular.module('footFall')
        .controller('CustomersController', CustomersController);

    CustomersController.$inject = ['$scope', '$rootScope', 'localStorageService', '$timeout'];

    function CustomersController($scope, $rootScope, localStorageService, $timeout) {

      $scope.viewTitle = "Customers";

      // listen for the $ionicView.enter event:
      $scope.$on('$ionicView.enter', function(e) {});

        var inStore = localStorageService.get('customers');
        $rootScope.customers = inStore || [];

        $scope.$watch('customers', function () {
          localStorageService.set('customers', $rootScope.customers);
        }, true);

      // List Toggles
      $scope.editBtnText = 'Remove';
      $scope.toggleDelete = function() {
        $scope.isDeletingItems = !$scope.isDeletingItems;
        $scope.isReorderingItems = false;
        $scope.editBtnText = ($scope.isDeletingItems ? 'Done' : 'Delete');
      };

      $scope.reorderBtnText = 'Reorder';
      $scope.toggleReorder = function() {
        $scope.isReorderingItems = !$scope.isReorderingItems;
        $scope.isDeletingItems = false;
        $scope.reorderBtnText = ($scope.isReorderingItems ? 'Done' : 'Reorder');
      };

      $scope.moveItem = function(item, fromIndex, toIndex) {
        //Move the item in the array
        $rootScope.customers.splice(fromIndex, 1);
        $rootScope.customers.splice(toIndex, 0, item);
        localStorageService.set('customers', $rootScope.customers);
      };

      $scope.removeCustomer = function(item) {
        if($rootScope.customers.length < 1) {
          $scope.editBtnText = 'Delete';
          console.log($rootScope.customers.length);
        }
        console.log('onDelete from the "list" on-delete attribute', item);
        $rootScope.customers.splice($rootScope.customers.indexOf(item), 1);
      };

    }

})();
