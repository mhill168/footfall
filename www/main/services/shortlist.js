/**
 * Created by mhill168 on 14/07/15.
 */

(function () {

  angular.module('glits').factory('shortlists', function ($ionicLoading, $timeout) {

    var shortlists = {};
    shortlists.list = [];

    shortlists.add = function (stand) {
      $timeout(function () {
        $ionicLoading.hide();
        shortlists.list.push({
          id: stand.id,
          icon: stand.icon,
          name: stand.name,
          team: stand.team,
          lead: stand.lead,
          umbrella: stand.umbrella
        });
      }, 250);
    };

    shortlists.remove = function (index) {
      $timeout(function () {
        $ionicLoading.hide();
        shortlists.list.splice(shortlists.list.indexOf(index), 1);
        console.log(shortlists.list);
      }, 250);

    };

    return shortlists;
  });

})();

