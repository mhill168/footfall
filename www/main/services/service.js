/**
 * Created by mhill168 on 08/07/15.
 */

(function () {

    'use strict';

    angular.module('glits').factory('glitzService', ['$http', '$q', '$timeout', glitzService]);

    function glitzService($http, $q, $timeout) {

        var appHeaders = {
            headers : {
                'Content-Type' : 'application/json; charset=UTF-8'
            }
        };

        function getFeedData() {

            var vm = this;

            var deferred = $q.defer();

            vm.data = {
                isLoading : true
            };

            $http.get("main/services/data/users.json", appHeaders, { cache : true } )
                .success(function (data) {
                    $timeout(function() {
                        console.log("HTTP call went as planned");
                        vm.data = {
                          isLoading : false
                        };
                        deferred.resolve(data);
                    },1000);
                })
                .error(function (status, error) {
                    console.log("Error while making HTTP call" + "Status is: " + status + "The error was: " + error);
                    deferred.reject();
                });

            return deferred.promise;
        }

        return {
            getFeedData: getFeedData
        };

    }

})();


